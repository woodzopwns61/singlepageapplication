const news = [
    {
        title: 'Vladimir Putin Rides a Bear',
        summary: 'A photo here shows that Vladimir Putin rode a bear last summer as part of a propaganda act.',
        date: new Date(2018, 11, 20),
        img: 'vlad.jpg'
    },
    {
        title: 'Boris Johnson, Exists',
        summary: 'How does Boris Johnson even exist anymore? I mean seriously?',
        date: new Date(2019, 10, 7),
        img: 'boris.jpg'
    },
    {
        title: 'The Queen, still alive',
        summary: 'The Queen is in fact still alive.',
        date: new Date(2021, 9, 25),
        img: 'queen.jpg'
    }];

placeArticles();

//document.fragment
//wrap elements
function placeArticles(){
    for (let i = 0; i < news.length; i++) {
        const para = document.createElement('ul');
        para.setAttribute('id', `newsList${i.toString()}`);
        const element = document.getElementById('div1');
        element.appendChild(para);

        const containerDiv = document.createElement('div');
        containerDiv.setAttribute('class', 'container');
        containerDiv.setAttribute('id', `container${i.toString()}`)
        const el = document.getElementById(`newsList${i.toString()}`);
        el.appendChild(containerDiv);

        const cross = document.createElement('button');
        const text = document.createTextNode('✖');
        cross.appendChild(text);
        cross.addEventListener('click', function () {document.getElementById(`newsList${i.toString()}`).remove()});
        const elem = document.getElementById(`container${i.toString()}`);
        elem.appendChild(cross);

        const img = document.createElement('img');
        img.src = news[i].img;
        const article = document.getElementById(`container${i.toString()}`);
        article.appendChild(img);

        const para1 = document.createElement('h3');
        const node1 = document.createTextNode(news[i].title);
        para1.appendChild(node1);
        const element1 = document.getElementById(`container${i.toString()}`);
        element1.appendChild(para1);

        const para2 = document.createElement('p');
        const node2 = document.createTextNode(news[i].summary);
        para2.appendChild(node2);
        const element2 = document.getElementById(`container${i.toString()}`);
        element2.appendChild(para2);

        const para3 = document.createElement('p');
        const node3 = document.createTextNode(news[i].date.toString());
        para3.appendChild(node3);
        const element3 = document.getElementById(`container${i.toString()}`);
        element3.appendChild(para3);
    }
}

function deleteAll() {
    for (let i = 0; i < news.length; i++){
        document.getElementById(`newsList${i.toString()}`).remove();
    }
    sortDate();
}

function sortDate() {
    news.sort((a, b) => b.date - a.date);
    placeArticles();
}

const button = document.getElementById('sortDate');
button.addEventListener('click', deleteAll);