let input, upper, ul, p, i, txt;

function search(){
    input = document.getElementById('input');
    upper = input.value.toUpperCase();
    ul = document.getElementsByTagName('ul');

    for (i = 0; i < ul.length; i++){
        p = ul[i].getElementsByTagName('p')[0];
        txt = p.textContent || p.innerText;

        if(txt.toUpperCase().indexOf(upper) > -1) {
            ul[i].style.display = '';
        } else {
            ul[i].style.display = 'none';
        }
    }
}